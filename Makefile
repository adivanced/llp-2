main: parser.tab.o ast_tree.o array.o main.o lex.yy.o
	gcc parser.tab.o ast_tree.o array.o main.o lex.yy.o -o main -lfl -lm

parser.tab.o: parser.y
	bison -d parser.y
	gcc parser.tab.c -c -lfl

lex.yy.o: lexer.l
	flex lexer.l
	gcc lex.yy.c -c -lfl

ast_tree.o: ast_tree.c ast_tree.h array.h
	gcc ast_tree.c -c

array.o: array.c array.h
	gcc array.c -c

main.o: main.c
	gcc main.c -c